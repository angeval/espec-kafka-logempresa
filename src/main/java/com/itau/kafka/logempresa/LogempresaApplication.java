package com.itau.kafka.logempresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogempresaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogempresaApplication.class, args);
	}

}
